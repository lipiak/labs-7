﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Lab7.AddressControl.Contract
{
    public interface IGetWindow
    {
        Control GetControl();
    }
}
