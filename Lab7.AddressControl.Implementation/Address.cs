﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab7.AddressControl.Contract;
using System.ComponentModel;

namespace Lab7.AddressControl
{
    public class Address : IAddress
    {
        AddressControl.AdressWindow aw;
        IGetWindow gw;
        ViewModel vm;

        public Address()
        {

        }

        public Address(IGetWindow gw)
        {
            this.gw = gw;
            aw = (AdressWindow)gw.GetControl();
            vm = (ViewModel)aw.DataContext;
            vm.PropertyChanged += this.onPropertyChanged;
        }

        public System.Windows.Controls.Control Control
        {
            get { return aw; }
        }

        public void onPropertyChanged(object sender, PropertyChangedEventArgs ex)
        {
            if (ex.PropertyName == "Address")
            {
                AddressChangedArgs aca = new AddressChangedArgs();
                aca.URL = vm.Address;
                AddressChanged.Invoke(this, aca);
            }
        }

        public event EventHandler<AddressChangedArgs> AddressChanged;
    }
}
