﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Lab7.AddressControl.Contract;

namespace Lab7.AddressControl
{
    class ViewModel:INotifyPropertyChanged
    {
        string _address;
        public string Address
        {
            get { return _address; }
            set { _address = value; OnPropertyChanged("Address"); }
        }

        private void OnPropertyChanged(String PropName)
	    {
	        if(PropertyChanged != null)
	            PropertyChanged(this, new PropertyChangedEventArgs(PropName));
	    }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
