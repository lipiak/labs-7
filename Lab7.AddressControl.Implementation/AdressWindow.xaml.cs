﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab7.AddressControl.Contract;

namespace Lab7.AddressControl
{
    /// <summary>
    /// Interaction logic for AdressWindow.xaml
    /// </summary>
    partial class AdressWindow : Window, IGetWindow
    {
        public AdressWindow()
        {
            this.DataContext = new ViewModel();
            InitializeComponent();
        }

        public Control GetControl()
        {
            return this.GetControl();
        }
    }
}
