﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab7.RemoteImageControl.Contract;
using Lab7.AddressControl.Contract;
using System.IO;
using System.Windows.Media;
using System.Net;

namespace Lab7.RemoteImageControl.Implementation
{
    public class ImageLoad : IRemoteImage
    {
        IAddress address;
        ImageLoader il;

        public ImageLoad(IAddress address)
        {
            this.address = address;
            il = new ImageLoader();
            address.AddressChanged += this.onAddressChange;
        }

        public System.Windows.Controls.Control Control
        {
            get { return il; }
        }

        void onAddressChange(object sender, AddressChangedArgs e)
        {
            Load(e.URL);
        }

        

        public void Load(string url)
        {
            MemoryStream ms = new MemoryStream(new WebClient().DownloadData(new Uri(url)));
            ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
            ImageSource imageSource = (ImageSource)imageSourceConverter.ConvertFrom(ms);
            il.obrazek.Source = imageSource;
        }
    }
}
